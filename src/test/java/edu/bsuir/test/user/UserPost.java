package edu.bsuir.test.user;

import edu.bsuir.pojo.User;
import edu.bsuir.test.BasicTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class UserPost extends BasicTest {

    private User postUser;
    private User updateUser;

    @Before
    public void setUp() {
        postUser = createDummyUser();
    }
    public void setUpUpdate() {
        updateUser = createNewUser();
    }


    @Test
    public void shouldCreateUser() {
        String userLocation = createResource("user/", postUser);
        User getUser = getResource(userLocation, User.class);
        assertThat(getUser).isEqualToIgnoringGivenFields(postUser, "id");
    }

    @After
    public void shutDown() {
        //TODO: delete dummy user
    }

    @Test
    public void shouldDeleteUser() {
        String userLocation = createResource("user/", postUser);
        User getUser = getResource(userLocation, User.class);
        User deleteUser = deleteResource(userLocation, getUser);
        assertThat(deleteUser).isEqualToIgnoringGivenFields(postUser, "id");
    }


    @Test
    public void shouldUpdateUser() {
        String userLocation = createResource("user/", postUser);
        postUser=updateUser;
        User updateU = updateResource(userLocation, User.class);

        assertThat(updateU).isEqualToIgnoringGivenFields(updateUser, "id");
    }



    private User createDummyUser() {
        return new User().setName("John")
                .setAge(10)
                .setSalary(3000);
    }

    private User createNewUser() {
        return new User().setName("John")
                .setAge(20)
                .setSalary(4000);
    }

}
